
restart: stop start

restart-build: stop start-build

start-build:
	echo building docker containers
	docker-compose up --build -d

start:
	echo starting docker containers
	docker-compose up -d

stop:
	echo gracefully stop docker containers
	docker-compose stop

kill:
	echo killing old docker processes
	docker-compose rm -fs